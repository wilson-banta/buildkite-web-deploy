var async = require('async');
var AWS = require('aws-sdk');
var ssm = new AWS.SSM();
var ec2 = new AWS.EC2();

exports.run = function(options, cb) {

    if (!options) throw "Options not supplied";
    if (!options.commandName) throw "Option.commandName not supplied";

    var context = {
        parameters: options.parameters,
        commandName: options.commandName,
        commandComment: options.commandComment || 'Deployment pipeline command',
        commandTimeout: options.commandTimeout || (5 * 60) /* 5 mins */ ,
        instanceStaAppsTagValues: options.instanceStaAppsTagValues,
        commandOutputS3BucketName: options.commandOutputS3BucketName || 'sta1-ec2-commands',
        commandOutputS3KeyPrefix: options.commandOutputS3KeyPrefix || 'deploy-develop',
        commandOutputS3BucketRegion: options.commandOutputS3BucketRegion || 'us-east-1'
    };

    async.waterfall([
        get_instances_to_run_command_on(context),
        run_command(context),
        wait_for_completion(context),
        print_results_to_console(context)
    ], cb);
}


function print_results_to_console(context) {

    return function(next) {
        //download from s3
        setTimeout(function() {

            var ssmParams = {
                CommandId: context.commandId,
                Details: true
            };

            ssm.listCommandInvocations(ssmParams, function(err, data) {

                if (err) {
                    console.log('ssm:listCommandInvocations failed');
                    console.log(err, err.stack);
                    throw err;
                }


                if (!data.CommandInvocations) {
                    throw 'No commnand output returned';
                }

                var s3 = new AWS.S3({
                    'region': context.commandOutputS3BucketRegion
                });
                var asyncParams = [];

                for (var x = 0; x < data.CommandInvocations.length; ++x) {
                    var commandInvocation = data.CommandInvocations[x];
                    for (var y = 0; y < commandInvocation.CommandPlugins.length; ++y) {
                        var commandPlugin = commandInvocation.CommandPlugins[y];

                        asyncParams.push({
                            s3Param: {
                                Bucket: commandPlugin.OutputS3BucketName,
                                Key: commandPlugin.OutputS3KeyPrefix + '/stdout.txt'
                            },
                            instanceId: commandInvocation.InstanceId
                        });
                    }
                }

                async.each(asyncParams,
                    function(asyncParam, callback) {
                        s3.getObject(asyncParam.s3Param, function(err, s3Data) {

                            if (err) {
                                console.log('s3:getObject failed');
                                console.log('Error downloading the output from: %s\\%s - $s', commandPlugin.OutputS3BucketName, commandPlugin.OutputS3KeyPrefix, err)
                                throw err;
                            }

                            if (!s3Data.Body) {
                                throw 'No commnand output returned';
                            }

                            console.log('Command output for instance id: %s\n%s', asyncParam.instanceId, s3Data.Body.toString());

                            callback();
                        });
                    },
                    function(err) {
                        next();
                    }
                );
            });
        }, 3000)
    }
}

function wait_for_completion(context) {

    return function(next) {

        var maxChecks = 10; //Should be set to command timeout property
        var checks = 0;
        var checkingStarted = false;
        console.log("Query command status using ID, and call back one completed. (pass an error if command doesn't run succesfully) ");

        var ssmParams = {
            CommandId: context.commandId
        };

        var interval = setInterval(function() {

            var currentStatus;
            if (!checkingStarted) {
                checkingStarted = true;

                ssm.listCommands(ssmParams, function(err, data) {

                    if (err) {
                        console.log('ssm:listCommands failed');
                        console.log(err, err.stack);
                        throw err;
                    }

                    checks = checks + 1;

                    currentStatus = data.Commands[0].Status;

                    console.log(`Current command status: ${currentStatus} (CommandID: ${context.commandId}) `);
                    if (currentStatus !== "Success" && currentStatus !== "InProgress" && currentStatus !== "Pending") {
                        clearInterval(interval);
                        throw "Error executing command. Status: " + currentStatus
                    }

                    if (checks > 50 || (currentStatus == "Success")) {
                        console.log(`Done, Checks: ${checks} command status: ${currentStatus} (CommandID: ${context.commandId}) `);
                        clearInterval(interval);
                        next();
                    }
                    checkingStarted = false;
                });
            }

        }, 8000);
    };
}

function run_command(context) {

    return function(next) {

        var instanceIds = [];

        for (var i = 0; i < context.instances.length; i++) {
            instanceIds.push(context.instances[i].InstanceId);
        }

        var params = {
            DocumentName: context.commandName,
            InstanceIds: instanceIds,
            Parameters: context.parameters,
            Comment: context.commandComment,
            TimeoutSeconds: context.commandTimeout,
            OutputS3BucketName: context.commandOutputS3BucketName,
            OutputS3KeyPrefix: context.commandOutputS3KeyPrefix
        };

        console.log('Sending Command: %j', params);

        ssm.sendCommand(params, function(err, data) {

            if (err) {
                console.log('ssm:sendCommand failed');
                console.log(err, err.stack);
                throw err;
            }

            context.commandId = data.Command.CommandId;
            console.log('command data', data)
            next();
        });


    }
}

function get_instances_to_run_command_on(context) {

    return function(next) {

        var filters = [];
        var params = {};
        if (context.instanceStaAppsTagValues) {
            filters.push({
                Name: 'tag-key',
                Values: ['STA-APPS'] // context.instanceStaAppsTagValues.split(',')
            })

            params = {
                Filters: filters
            };
        }


        ec2.describeInstances(params, function(err, data) {

            if (err) {
                console.log('ec2:describeInstances failed');
                console.log(err, err.stack);
                throw err;
            }

            if (!data.Reservations) {
                throw 'No Reservations returned';
            }

            var instances = [];
            for (var i = 0; i < data.Reservations.length; ++i) {
                for (var x = 0; x < data.Reservations[i].Instances.length; ++x) {

                    var tags = data.Reservations[i].Instances[x].Tags;

                    for (var t = 0; t < tags.length; t++) {
                        if (tags[t].Key === 'STA-APPS') {
                            console.log(`Check: ${tags[t].Value} IndexOf: ${context.instanceStaAppsTagValues} is: ${tags[t].Value.indexOf(context.instanceStaAppsTagValues)}`)
                            if (tags[t].Value.indexOf(context.instanceStaAppsTagValues) >= 0) {
                                instances.push(data.Reservations[i].Instances[x]);
                            }

                        }
                    }
                }
            }

            context.instances = instances;

            if (context.instances.length == 0) throw 'No instances tagged with: ' + context.instanceStaAppsTagValues;
            next();
        });
    };
}
