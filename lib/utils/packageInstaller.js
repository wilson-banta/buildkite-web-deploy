var ssm_command = require('../run_ssm_command');

module.exports.install = function(options, cb) {

    if (!options) throw new Error('Options must be supplied');

    if (!options.packageName) throw new Error('packageName must be supplied');
    if (!options.environment) throw new Error('environment must be supplied');

    var params = {
      packageName: [options.packageName],
      environment: [options.environment],
      noTimeBasedSubDirectory: options.noTimeBasedSubDirectory ? ['true'] : null,
    }
    //run ssm command
    ssm_command.run({
        commandName: 'STA-prazzle-deploy-package',
        commandComment: options.comment || `Running: ${options.packageName} - Env: ${options.environment} `,
        parameters: params,
        instanceStaAppsTagValues: options.packageName
    }, cb);
}
