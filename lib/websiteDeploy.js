var ssm_command = require('./run_ssm_command');
var async = require('async');
var loadBalancer = require('./utils/loadBalancer');
var installer = require('./utils/packageInstaller');
var log = require('winston');

module.exports = function(options, cb) {


    if (!options) throw new Error('Options must be supplied');
    if (!options.loadBalancerExternalPort) throw new Error('loadBalancerExternalPort must be supplied');
    if (!options.loadBalanceInstanceGreenPort) throw new Error('loadBalanceInstanceGreenPort must be supplied');
    if (!options.loadBalanceInstanceBluePort) throw new Error('loadBalanceInstanceBluePort must be supplied');
    if (!options.loadBalancerName) throw new Error('loadBalancerName must be supplied');
    if (!options.environment) throw new Error('environment must be supplied');

    //  if (!options.packages || !options.packages.length > 0) {
    //    throw new Error('Packages to install must be supplied');
    //  }

    async.waterfall([
            get_current_load_balancer_instance_port(options),
            determine_sub_environment_name(options),
            deploy_all_packages(options)
        ],
        function(err) {
            if (err) throw err;
            console.log('Deployment complete');
            cb();
        }
    );
}

function deploy_all_packages(options) {
    return (cb) => {

        log.info(`Deploying Apps ${options.subEnvironmentName}, port: ${options.loadBalanceNextInstancePort}`);

        async.parallel(
            [
                deploy_background_worker(options),
                function(cb) {
                  //Cant do these in parallel because IIS installer changes the application.config file and has conflict errors
                    async.waterfall([
                        deploy_adminweb(options),
                        deploy_public_site(options),
                        deploy_organiser_web(options)
                    ], cb);
                }
            ],
            cb
        );
    }
}

function deploy_public_site(options) {
    return cb => {

        var params = {
            packageName: ['PublicSite'],
            environment: [options.environment],
            siteName: [`PublicSite-${options.subEnvironmentName}`],
            directoryName: [`PublicSite-${options.subEnvironmentName}`],
            port: [""+options.loadBalanceNextInstancePort],
            hostHeader: ["${HostHeader}"]
        };

        //run ssm command
        ssm_command.run({
            commandName: 'STA-prazzle-deploy-web-package',
            commandComment: `Install ${params.siteName} - Env: ${options.environment}`,
            parameters: params,
            instanceStaAppsTagValues: params.packageName[0]
        }, cb);
    }
}

function deploy_organiser_web(options) {
    return cb => {

        var params = {
            packageName: ['Web'],
            environment: [options.environment],
            siteName: [`PublicSite-${options.subEnvironmentName}`],
            directoryName: [`Web-${options.subEnvironmentName}`],
            port: [""+options.loadBalanceNextInstancePort],
            hostHeader: ["${HostHeader}"]
        };

        //run ssm command
        ssm_command.run({
            commandName: 'STA-prazzle-deploy-web-package',
            commandComment: `Install ${params.packageName}/organiser - Env: ${options.environment}`,
            parameters: params,
            instanceStaAppsTagValues: params.packageName[0]
        }, cb);
    }
}

function deploy_adminweb(options) {
    return cb => {

        var params = {
            packageName: ['AdminWeb'],
            environment: [options.environment],
            siteName: [`AdminWeb-${options.subEnvironmentName}`],
            directoryName: [`AdminWeb-${options.subEnvironmentName}`],
            port: [""+options.loadBalanceNextInstancePort],
            hostHeader: ["${HostHeader}"]
        };

        //run ssm command
        ssm_command.run({
            commandName: 'STA-prazzle-deploy-web-package',
            commandComment: `Install ${params.siteName} - Env: ${options.environment}`,
            parameters: params,
            instanceStaAppsTagValues: params.packageName[0]
        }, cb);
    }
}

function deploy_background_worker(options) {
    return cb => {
        var params = {
            packageName: ['BackgroundWorker'],
            environment: [options.environment]
        };

        //run ssm command
        ssm_command.run({
            commandName: 'STA-prazzle-deploy-package',
            commandComment: `Install ${params.packageName} - Env: ${options.environment}`,
            parameters: params,
            instanceStaAppsTagValues: params.packageName[0]
        }, cb);
    }
}

function get_current_load_balancer_instance_port(options) {
    return (cb) => {
        loadBalancer.getInstancePort({
                loadBalancerExternalPort: options.loadBalancerExternalPort,
                loadBalancerName: options.loadBalancerName
            },
            function(err, r) {
                if (err) return cb(err);
                log.info('Current load balancer instance port is : ' + r)
                options.currentLoadBalancerInstancePort = r;
                cb()
            });
    }
}


function determine_sub_environment_name(options) {
    return function(cb) {

        if (options.currentLoadBalancerInstancePort == options.loadBalanceInstanceGreenPort) {
            options.subEnvironmentName = 'Blue'
            options.loadBalanceNextInstancePort = options.loadBalanceInstanceBluePort;
        } else {
            options.subEnvironmentName = 'Green'
            options.loadBalanceNextInstancePort = options.loadBalanceInstanceGreenPort;
        }

        log.info('Sub environment is : ' + options.subEnvironmentName);
        return cb();

        //cb('Load balancer is configured on an unknown instance port');
    };
}

/*




Get Load Balancer Port Mapping for port 80
if current port is 8010 then deploy to 8020
Rum SSM Command to deploy apps



console.log('Deploy Apps');

var portsToEnvMap = {
    8010: 'Blue',
    8020: 'Green'
};

var options = {
    loadBalancerExternalPort: 80,
    packages:
    {
        'Web': '3.20.0',
        'PublicSite': '3.20.0'
    }
};


async.waterfall([
        get_load_balancer_port(options),
        determine_sub_environment_name(options);
        run_install(options)
    ],
    function(err) {
        if (err) throw err;
        Console.log('Deployment complete');
    }
);


function run_install(options) {
    return function(cb) {
        var webDeploy = {
            name: 'Web',
            instanceName: 'Web_' + options.instanceName,
            version:
        };

        console.log(`prazzle deploy --name Web --instanceName Web`)
    }
}


function determine_sub_environment_name(options, cb) {
    return function(cb) {
        options.subEnvironmentName = portsToEnvMap[options.loadBalancerInstancePort];
        cb();
    };
}

function get_load_balancer_port(options, cb) {
    return function(cb) {
        options.loadBalancerInstancePort = 8010;
        cb();
    };
}

*/
