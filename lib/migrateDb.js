var log = require('winston');
var async = require('async');
var installer = require('./utils/packageInstaller');
var run_ssm_command = require('../lib/run_ssm_command');

module.exports = function(options, cb) {

    if (!options) throw new Error('Options must be supplied');
    if (!options.environment) throw new Error('environment must be supplied');


    var context = {};
    async.waterfall([
            deploy_database_package(options, context),
            run_ssm_deploy_database_command(options, options)
        ],
        function(err) {
            if (err) throw err;
            console.log('Database Migration complete');
            cb();
        }
    );
}

function deploy_database_package(options, context) {
    return (cb) => {

        log.info(`Deploying Database Package`);

        var installParams = {
            packageName: 'Database',
            environment: options.environment,
            instanceStaAppsTagValues: 'Database',
            noTimeBasedSubDirectory: '1'
        };

        installer.install(installParams, cb);
    }
}

function run_ssm_deploy_database_command(options, context) {
    return (cb) => {
        var params = {
            commandName: 'STA-web-migrate-database',
            commandComment: 'Migrating Database',
            instanceStaAppsTagValues: 'Database',
            parameters: {environment: [options.environment] },
            commandTimeout: 60 * 5 // 5 mins
        };

        run_ssm_command.run(params, cb);
    }
}
