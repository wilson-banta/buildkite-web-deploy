console.log('Switch Load Balancer');

var loadBalancer = require('./lib/utils/loadBalancer');
var async = require('async');

var environmentName = process.env.ENVIRONMENT_NAME;
var loadBalancerName = process.env.LOAD_BALANCER_NAME;

if (!environmentName) throw 'An ENVIRONMENT_NAME environment variable needs to be set';
if (!loadBalancerName) throw 'An LOAD_BALANCER_NAME environment variable needs to be set';


async.waterfall([
    switch_public_site()
    ,
    switch_admin_site()
], function(e) {
    if (e) throw e;
    console.log('Deployent of Apps')
});


function switch_public_site() {
    return cb => {
        loadBalancer.switchInstancePort({
            loadBalancerExternalPort: 80,
            loadBalancerExternalPort2: 443,
            loadBalanceInstanceGreenPort: 8010,
            loadBalanceInstanceBluePort: 8020,
            loadBalancerName: loadBalancerName,
            environment: environmentName
        }, cb);
    }
}


function switch_admin_site() {
    return cb => {
        loadBalancer.switchInstancePort({
            loadBalancerExternalPort: 4443,
            loadBalanceInstanceGreenPort: 8010,
            loadBalanceInstanceBluePort: 8020,
            loadBalancerName: loadBalancerName,
            environment: environmentName
        }, cb);
    }
}
