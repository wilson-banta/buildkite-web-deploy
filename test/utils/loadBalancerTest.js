var loadBalancer = require('../../lib/utils/loadBalancer');
var assert = require('better-assert');



describe('loadBalancer', function() {


    describe('getInstancePort', () => {

        var validRequest;

        function createValidRequest() {
            return {
                loadBalancerExternalPort: 80,
                loadBalancerName: 'Staging-ELB'
            };
        }

        beforeEach(function() {
            validRequest = createValidRequest();
        });

        describe('input validation', () => {

            function performRequest(errorMessage) {
                if (!doesThrow(() => loadBalancer.getInstancePort(validRequest), errorMessage)) throw errorMessage;
            }

            it("should require external port", () => {
                validRequest.loadBalancerExternalPort = null;
                performRequest('loadBalancerExternalPort must be supplied');
            });

            it("should require load balancer name", () => {
                validRequest.loadBalancerName = null;
                performRequest('loadBalancerName must be supplied');
            });

        });

        describe('valid request', () => {

            it("should return the internal port", () => {

                loadBalancer.getInstancePort(validRequest, function(err, r) {
                    assert(r == 80 || r == 8010 || r == 8020);
                });
            });
        });
    })

    describe('switchInstancePort', () => {

        var validRequest;

        function createValidRequest() {
            return {
                loadBalancerExternalPort: 80,
                loadBalancerName: 'Staging-ELB',
                loadBalanceInstanceGreenPort: 8010,
                loadBalanceInstanceBluePort: 8020,
            };
        }

        beforeEach(function() {
            validRequest = createValidRequest();
        });

        describe('input validation', () => {

            function performRequest(errorMessage) {
                if (!doesThrow(() => loadBalancer.switchInstancePort(validRequest), errorMessage)) throw errorMessage;
            }

            it("should require load balancer name", () => {
                validRequest.loadBalancerName = null;
                performRequest('loadBalancerName must be supplied');
            });

            it("should require external port", () => {
                validRequest.loadBalancerExternalPort = null;
                performRequest('loadBalancerExternalPort must be supplied');
            });

            it("should require load balancer instance green port", () => {
                validRequest.loadBalanceInstanceGreenPort = null;
                performRequest('loadBalanceInstanceGreenPort must be supplied');
            });

            it("should require load balancer instance blue port", () => {
                validRequest.loadBalanceInstanceBluePort = null;
                performRequest('loadBalanceInstanceBluePort must be supplied');
            });

        });

        describe('valid request', () => {


            it("should be able to switch the port", (cb) => {

                this.timeout(100000);


                loadBalancer.getInstancePort(validRequest, function(err, p1) {

                    if (err) throw err;

                    console.log('Load Balancer Instance Port: ' + p1);

                    loadBalancer.switchInstancePort(validRequest, (err) => {
                        if (err) throw err;

                        loadBalancer.getInstancePort(validRequest, function(err, p2) {
                            if (err) throw err;
                            console.log('Load Balancer Instance Port: ' + p2);
                            assert(p1 != p2);
                            assert(p2 == validRequest.loadBalanceInstanceGreenPort || p2 == validRequest.loadBalanceInstanceBluePort);

                            return cb();
                            loadBalancer.switchInstancePort(validRequest, (err) => {
                                if (err) throw err;

                                loadBalancer.getInstancePort(validRequest, function(err, p3) {
                                    console.log('Load Balancer Instance Port: ' + p3);
                                    assert(p3 != p2);

                                    assert(p3 == validRequest.loadBalanceInstanceGreenPort || p3 == validRequest.loadBalanceInstanceBluePort);

                                    cb();
                                });

                            });
                        });
                    });
                });
            });
        });
    })
});
