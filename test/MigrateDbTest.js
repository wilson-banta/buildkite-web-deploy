var assert = require('better-assert');
var sinon = require('sinon');
var installer = require('../lib/utils/packageInstaller');
var run_ssm_command = require('../lib/run_ssm_command');
var migrateDb = require('../lib/migrateDb');


function createValidRequest() {
    return {
        environment: 'test'
    };
}
describe('migrateDb', function() {

    var validRequest;

    beforeEach(function() {
        validRequest = createValidRequest();
    });

    describe('parameter error checks', function() {

        function performRequest(errorMessage) {
            if (!doesThrow(() => migrateDb(validRequest), errorMessage))
                throw errorMessage;
        }

        it('should throw if environment is not supplied', function() {
            validRequest.environment = null;
            performRequest('environment must be supplied');

        });
    });

    describe('valid request', function() {

        var packageName, environment;
        var commandOptions;

        beforeEach(function() {
            sinon.stub(installer, 'install', function(options, cb) {
                packageName = options.packageName;
                environment = options.environment;
                return cb();
            });

            sinon.stub(run_ssm_command, 'run', function(options, cb) {
                commandOptions = options;
                cb();
            });
        });

        it('should run the install for the database package', function(cb) {

            migrateDb(validRequest, function() {

                assert(packageName == 'Database');
                assert(environment == validRequest.environment);
                cb();
            })
        });

        it('should run the install database ssm command', function(cb) {

            migrateDb(validRequest, function() {

                assert(commandOptions.commandName == 'STA-Migrate-Database');
                assert(commandOptions.tagged);
                assert(commandOptions.tagged.key == 'STA-Apps');
                assert(commandOptions.tagged.value == 'Database');
                
                cb();
            })
        });

        afterEach(function() {
            installer.install.restore();
            run_ssm_command.run.restore();
        });
    });
});
