var async = require('async');

console.log('Deploying the Apps');


var environmentName = process.env.ENVIRONMENT_NAME;
var loadBalancerName = process.env.LOAD_BALANCER_NAME;

if (!environmentName) throw 'An ENVIRONMENT_NAME environment variable needs to be set';
if (!loadBalancerName) throw 'An LOAD_BALANCER_NAME environment variable needs to be set';


var websiteDeploy = require('./lib/websiteDeploy');

async.parallel([
    deploy_public_site
    //,    deploy_admin_site
], function(e) {
    if (e) throw e;
    console.log('Deployent of Apps')
})


function deploy_public_site(cb) {
    websiteDeploy({
        loadBalancerExternalPort: 80,
        loadBalanceInstanceGreenPort: 8010,
        loadBalanceInstanceBluePort: 8020,
        loadBalancerName: loadBalancerName,
        environment: environmentName
    },cb);
}
/*
function deploy_admin_site(cb) {
    websiteDeploy({
        loadBalancerExternalPort: 4443,
        loadBalanceInstanceGreenPort: 8010,
        loadBalanceInstanceBluePort: 8020,
        loadBalancerName: loadBalancerName,
        environment: environmentName + '-Web',
        packages: [
            'AdminWeb'
        ]
    },cb);
}
*/
