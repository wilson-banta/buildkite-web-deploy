console.log('Migrating the Database');

var environmentName = process.env.ENVIRONMENT_NAME;

if (!environmentName)
    throw 'An ENVIRONMENT_NAME environment variable needs to be set';

var migrateDb = require('./lib/migrateDb');

migrateDb({
    environment: environmentName
}, e => {
    if (e) throw e;
    console.log('Migration of Database Complete')
});
